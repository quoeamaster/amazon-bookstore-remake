// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// amazon bookstore remake vite + vue3
// Copyright (C) 2021,2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import axios from 'axios';

// [idea] -> https://chafikgharbi.com/vuejs-global-function/
// [lesson] -> https://vuejs.org/guide/reusability/plugins.html
export default {
   install: (app, options) => {
      // inject global util methods
      app.config.globalProperties.$post = async (url, data) => {
         // either data is provided or just an empty {}
         let _data = (data || {});
         let _url = `http://localhost:3001/api${url}`;
         try {
            let _result = await axios({
               method: 'post',
               url: _url,
               data, _data,
               withCredentials: true,
            });
            return _result;
         } catch(e) {
            // axios wrapped error
            if (null != e.response) {
               return {
                  code: e.response.status,
                  errMsg: e.message,
               };
            }
            // non AXIOS wrapped, could be anything
            return {
               code: 500,
               errMsg: `${e}`,
            };
         }
      }; // end $post

      app.config.globalProperties.$get = async (url, data) => {
         // either data is provided or just an empty {}
         let _data = (data || {});
         let _url = `http://localhost:3001/api${url}`;
         try {
            // [approach] another way to create the queryString is through URLSearchParams(JSON_OBJECT);
            //const _params = new URLSearchParams(_data);
            let _result = await axios({
               method: 'get',
               url: _url,
               params: _data,
               withCredentials: true,
            });
            return _result;
         } catch(e) {
            // axios wrapped error
            if (null != e.response) {
               return {
                  code: e.response.status,
                  errMsg: e.message,
               };
            }
            // non AXIOS wrapped, could be anything
            return {
               code: 500,
               errMsg: `${e}`,
            };
         }
      }; // end $get

      
   },
}