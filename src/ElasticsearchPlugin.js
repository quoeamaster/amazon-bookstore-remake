// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// amazon bookstore remake vite + vue3
// Copyright (C) 2021,2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/**
 * _isStatus200 - checks whether the elasticsearch response is a 200 status.
 * 
 * @param {*} payload 
 * @returns 
 */
function _isStatus200(payload) {
   if (null != payload && null != payload.data) {
      const _data = payload.data;
      if (_data.code == 200) {
         return { '200': true, payload: _data.payload };
      }
   }
   return { '200': false, payload: {} };
};

export default {
   install: (app, options) => {
      // $getQueryHits - traverse the JSON object to extract the esearch hits array.
      // Extraction is run only if:
      // - payload                           is valid
      // - payload.data                      is valid
      // - payload.data.code                 == 200
      // - payload.data.payload              is valid
      // - payload.data.payload.hits.hits    is valid
      app.config.globalProperties.$getQueryHits = (payload) => {
         let _hits = {};
         const _is200 = _isStatus200(payload);

         if (true == _is200['200'] && null != _is200['payload']) {
            const _p = _is200.payload;
            if (null != _p.hits && null != _p.hits.hits) {
               _hits = _p.hits.hits;
            }
         }
         return _hits;
      }; // end $getQueryHits logic

      // $getMSearchHits - retrieve the msearch responses array.
      app.config.globalProperties.$getMSearchHits = (payload) => {
         let _hits = [];
         if (null != payload && null != payload.data) {
            const _data = payload.data;
            if (_data.code == 200 && null != _data.payload && null != _data.payload.responses) {
               _hits = _data.payload.responses;
            }
         }
         return _hits;
      }; // end $getQueryHits logic

      // $isHitFound - check whether the hit based on ID is found or not.
      // return { found: true|false, hit: JSON }
      app.config.globalProperties.$isHitFound = (payload) => {
         const _is200 = _isStatus200(payload);

         if (true == _is200['200'] && null != _is200['payload']) {
            const _p = _is200.payload;
            if (true == _p.found) {
               return { found: true, hit: _p }; 
            }
         }
         return { found: false };
      }; // end $isHitFound logic

      // $isValidESOperation - check whether the elasticsearch response is status 200. 
      // Also returns the "payload" associated with this response.
      app.config.globalProperties.$isValidESOperation = (payload) => {
         return _isStatus200(payload);
      };

   },
};