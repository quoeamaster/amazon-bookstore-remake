// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// amazon bookstore remake vite + vue3
// Copyright (C) 2021,2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import util from './sessionStoreUtil.js';

// [idea] -> https://chafikgharbi.com/vuejs-global-function/
// [lesson] -> https://vuejs.org/guide/reusability/plugins.html
export default {
   install: (app, options) => {
      // inject global util methods
      app.config.globalProperties.$isUserAuth = () => {
         return util.isUserAuth();
      };
      app.config.globalProperties.$isCartEmpty = () => {
         return util.isCartEmpty();
      };
      app.config.globalProperties.$setUserAuth = (value) => {
         util.setUserAuth(value);
      };
      app.config.globalProperties.$setUsername = (value) => {
         util.setUsername(value);
      };
      app.config.globalProperties.$getUsername = () => {
         return util.getUsername();
      };
      app.config.globalProperties.$setCart = () => {
         return util.setCart();
      };
      app.config.globalProperties.$unsetCart = () => {
         return util.unsetCart();
      };


   },
}