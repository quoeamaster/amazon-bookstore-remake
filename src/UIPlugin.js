// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// amazon bookstore remake vite + vue3
// Copyright (C) 2021,2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

export default {
   install: (app, options) => {
      // inject global util methods
      app.config.globalProperties.$bookCoverCheck = (img) => {
         if (null != img && null != img.target && 
            null != img.target.naturalHeight && img.target.naturalHeight == 1 && 
            null != img.target.naturalWidth && img.target.naturalWidth == 1) {

            img.target.src = '/notFoundBookCover.png';
         }
      };
      // fixing the position to avoid scrollable backgrounds when a modal dialog is displayed
      // [bug] seems the scrollable... has issues when body: {position: fixed|static} but at least at the moment fixed
      app.config.globalProperties.$fixedBodyPosition = (shouldFix) => {
         if (true == shouldFix) {
            window.document.body.style.position = "fixed";
            window.document.body.style.width = "100%";
            window.document.body.style.height = "100%";
         } else {
            window.document.body.style.position = "static";
            window.document.body.style.width = "100%";
            window.document.body.style.height = "100%";
         }
      };

   }
}