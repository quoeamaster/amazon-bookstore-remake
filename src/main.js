// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// amazon bookstore remake vite + vue3
// Copyright (C) 2021,2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { createApp } from 'vue'
import App from './App.vue'

import './index.css'
import 'tippy.js/dist/tippy.css'
import 'tippy.js/themes/light-border.css';

// setup store, router
import store from './Store'
import router from './Router'


/* import the fontawesome core */
import { library } from '@fortawesome/fontawesome-svg-core'

/* import specific icons */
import { faHouse, faChevronLeft, faChevronRight, faBook, faStar, faCircleInfo, faEnvelope, faCircleXmark, fa1, faCircle, 
   faCaretLeft, faCaretRight, faCaretDown, faCartShopping, faCartArrowDown, 
   faUser, faTrash, faSwatchbook, faBars } from '@fortawesome/free-solid-svg-icons'
import { faStar as regStar, faUser as regUser, faAddressCard as regAddrCard } from '@fortawesome/free-regular-svg-icons'
import { faPaypal, faCcPaypal, faCcAmazonPay, faCcApplePay, faCcStripe, faCcVisa, faGithub } from '@fortawesome/free-brands-svg-icons'

/* import font awesome icon component, also the "layers" for stacking up icons */
import { FontAwesomeIcon, FontAwesomeLayers } from '@fortawesome/vue-fontawesome'

/* add icons to the library */
library.add( faHouse, faPaypal, faCcPaypal, faCcAmazonPay, faCcApplePay, faCcStripe, faCcVisa, faChevronLeft, faChevronRight, faBook, faStar, regStar, faCaretLeft, faCircleInfo, faEnvelope, faCircleXmark, fa1, faCircle, 
   faCaretRight, faCaretDown, faCartShopping, faCartArrowDown, faUser, 
   regUser, faTrash, faGithub, regAddrCard, faSwatchbook, faBars);

/** [plugin test] */
import sessionStoragePlugin from './SessionStorePlugin.js';
import axiosPlugin from './AxiosPlugin.js';
import esPlugin from './ElasticsearchPlugin.js';
import uiPlugin from './UIPlugin.js';

createApp(App).
   component("fa-icon", FontAwesomeIcon).component('fa-layer', FontAwesomeLayers).
   use(store).
   use(router).
   use(sessionStoragePlugin).use(axiosPlugin).use(esPlugin).use(uiPlugin).mount('#app')
