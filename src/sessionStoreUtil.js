// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// amazon bookstore remake vite + vue3
// Copyright (C) 2021,2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/**
 * return the value associated with the [key] within the sessionStorage.
 * 
 * @param {*} key 
 * @returns 
 */
function get(key) {
   let _v = null;
   if (window.sessionStorage) {
      _v = window.sessionStorage.getItem(key);
   }
   return _v;
}

/**
 * set the [value] bound to the given [key] within the sessionStorage.
 * 
 * @param {*} key 
 * @param {*} value 
 */
function set(key, value) {
   if (window.sessionStorage) {
      window.sessionStorage.setItem(key, value);
   }
}

/**
 * remove the [key] item within the sessionStorage.
 * 
 * @param {*} key 
 */
function unset(key) {
   if (window.sessionStorage) {
      window.sessionStorage.removeItem(key);
   }
}

export default {
   // [constant] u_token = whether a JWT token is available (within httpOnly cookies though)
   // value - true | false
   U_TOKEN: 'has_u_token',
   // [constant] k_cart = the key associated with the shopping cart in the sessionStorage
   K_CART: 'k_cart',
   // [constant] u_name = the login username that succeeded
   U_NAME: 'u_name',

   // isUserAuth - if the u_token is available, assume authenticated already 
   // (production probably needs some timeout checks too - or let the server side code do the trick)
   isUserAuth: function() {
      const _v = get(this.U_TOKEN);
      if ('false' == _v || false == _v) {
         return false;
      } else if (null == _v || undefined == _v) {
         return false;
      }
      return true;
   },

   // isCartEmpty - checks whether the session's cart is empty or not.
   isCartEmpty: function() {
      const _v = get(this.K_CART);
      if (_v != null) {
         return false;
      }
      return true;
   },

   setUserAuth: function(value) {
      set(this.U_TOKEN, value);
      // [debug]
      //console.log(`@@@ after token update ${get(this.U_TOKEN)}`);
   },
   // setCart - mark the K_CART indicator to a non-null value (e.g. 'true'). 
   // Actual cart contents are retrieved through ES API(s) directly.
   setCart: function() {
      set(this.K_CART, true);
   },
   // unsetCart - remove the [k_cart] item within the sessionStorage.
   unsetCart: function() {
      unset(this.K_CART);
   },

   setUsername: function(value) {
      set(this.U_NAME, value);
   },
   getUsername: function() {
      return get(this.U_NAME);
   },

};
